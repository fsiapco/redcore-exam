<?php

namespace App\Models;

use App\Http\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserRole extends Model
{
    use HasFactory,UsesUuid;
    
    protected $fillable = [
        'uuid',
        'user_id',
        'role_id',
    ];

    public function role(){
        return $this->hasOne('App\Models\Role','id', 'role_id');
    }
}
