<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserRole;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterRequest;

class UserController extends Controller
{
    public function index()
    {
        return response()->json(UserResource::collection(User::with('userRole.role')->get()));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $user =  User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
       
        $user_role = UserRole::create([
            'user_id' => $user->id,
            'role_id' => $request['role_id']
        ]);
     
        return response()->json($user, 201);
    }

    public function update($id, Request $request)
    {
        
        $user = User::whereId($id)->update([
            'name' => $request['name'],
            'email' => $request['email'],
            
            'password' => Hash::make($request['password']),
        ]);

        $user_role = UserRole::UpdateOrcreate([
            'user_id' => $user->id,
            'role_id' => $request['role_id']
        ]);
        return response()->json($user);
    }

    public function show($id)
    {
        $user = User::find($id);

        return response()->json($user);
    }
    public function destroy($id)
    {
        $response = User::whereId($id)->delete();

        return response()->json($response);
    }
}
