<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;
use App\Http\Resources\RoleResource;

class RoleController extends Controller
{
    public function index()
    {
        return response()->json(RoleResource::collection(Role::get()));
    }

    public function store(RoleRequest $request)
    {
        $response = Role::create($request->only([
            'name'
        ]));

        return response()->json($response, 201);
    }

    public function show($id)
    {
        $role = Role::whereId($id)->first();

        return response()->json($role);
    }

    public function update($id, RoleRequest $request)
    {
        $role = Role::whereId($id)->update($request->only(['name']));

        return response()->json($role);
    }

    public function destroy($id)
    {
        $response = Role::whereId($id)->delete();

        return response()->json($response);
    }
}
