# RedCore Exam



## Getting started
Red core technical exam 

## Laravel coding Exam
    1.Laravel Backend

        o Create a simple user-role-permissions API template

        o Create 2 tables (users, roles)

        § Relationship (1 to 1 for user to role)

        § Users will have the following information

        § Full Name

        § Email Address

        § Nominated Password 

        § Confirmed Password

        § Roles will have the following information

        § Role Name

        § Description

        o Create CRUD API endpoint for users

        o Create CRUD API endpoint for roles

        o Create a login API endpoint

    2.VueJs Frontend 

        o  Add TypeScript to your frontend solution

        o   Create a simple login form

        § The following information should be processed during Login:

        § Email Address

        § Password

        § Page should:

        § Redirect to the homepage and display the Full Name of the logged-in user

        § Show Logout option once logged in

        § Show validation errors on the Login form

        o Create ROLE CRUD Operation  after the user has logged in (consume the API from the  laravel backend)   

        o Create USER CRUD Operation after the user has logged in (consume the API from the laravel backend) 

        § For the Create Users Operation, the following inputs should be captured;

        § Full Name

        § Email Address

        § Nominated Password

        § Confirm Password

        § Assign Role

        § The page should validate the following;

        § Check for duplicate email and name

        § Check the password against the following requirements

        § Minimum length: 8 characters

        § Confirm password is the same   

## Integrate with your tools

- xampp (database)
- composer (latest)
- nodejs (latest)
- vscode (latest)



## Deploy

    $ cp .env.example .env
    $ composer install
    $ php artisan key:generate
    $ php artisan jwt:secret
    $ php artisan migrate --seed
    $ php artisan config:cache
    $ php artisan route:cache

    php artisan serve ( to run the backend/api)
    npm run dev (to run frontend/vue)
***

#Test 
    Note: Do not run in production environment, this will reset the database.
    $ php artisan test



