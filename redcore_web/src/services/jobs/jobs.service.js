import axios from 'axios';
import authHeader from "@/services/auth/auth-header"

const API_URL = import.meta.env.VITE_API_URL;

class JobsService {
    getJobsDetails() {
        return axios.get(API_URL + '/api/jobs/9ff08eac-6a74-4e3f-b5b8-4976f950d3f1', {headers : authHeader()})
            .then((response) => {
                return response.data;
            });
    }
    getRecommendedJobs() {
        return axios.get(API_URL + '/api/applications?recommended=jobs&location=b5adf5a4-abe6-4785-8864-83e3e44b93fa', {headers : authHeader()})
            .then((response) => {
                return response.data;
            });
    }
    getRelatedJobs() {
        return axios.get(API_URL + '/api/jobs?related=philippines', {headers : authHeader()})
            .then((response) => {
                return response.data;
            });
    }
    getMostRecentJobs() {
        return axios.get(API_URL + '/api/jobs?sort_by=created_at&direction=desc&limit=3', {headers : authHeader()})
            .then((response) => {
                return response.data;
            });
    }

    getHighestPayingJob() {
        return axios.get(API_URL + '/api/jobs?sort_by=max_salary&direction=desc&limit=1', {headers : authHeader()})
            .then((response) => {
                return response.data;
            })
    }
    getTrendingJobs (){ //to fix
        return axios.get(API_URL + '/api/jobs?trending=decs&limit=5', {headers : authHeader()})
        .then((response) => {
            return response.data;
        })
    }
    searchJobs({data}){
        return axios.get(API_URL + '/api/jobs?search=' + data, {headers : authHeader()})
            .then((response) => {
               
                return response.data;
            })
    }

    location() {
        return axios.get(API_URL + '/api/locations', {headers : authHeader()})
        .then((response) => {
            return response.data;
        })
    }

    jobLevels() {
        return axios.get(API_URL + '/api/job-levels', {headers : authHeader()})
        .then((response) => {
            return response.data;
        })
    }

    jobTypes() {
        return axios.get(API_URL + '/api/job-types', {headers : authHeader()})
        .then((response) => {
            return response.data;
        })
    }

    
    education() {
        return axios.get(API_URL + '/api/educations', {headers : authHeader()})
        .then((response) => {
            return response.data;
        })
    }

    industry() {
        return axios.get(API_URL + '/api/industries', {headers : authHeader()})
        .then((response) => {
            return response.data.data;
        })
    }

    department() {
        return axios.get(API_URL + '/api/departments', {headers : authHeader()})
        .then((response) => {
            return response.data;
        })
    }

}

export default new JobsService();