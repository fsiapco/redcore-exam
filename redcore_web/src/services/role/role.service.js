import axios from 'axios';
import authHeader from "@/services/auth/auth-header"

const API_URL = import.meta.env.VITE_API_URL;

class RoleService {
    getAllRoles() {
        return axios.get(API_URL + '/api/roles', {headers : authHeader()})
            .then((response) => {
                return response.data;
            });
    }
    createNewRoles(data){
        console.log(data);
        return axios.post(API_URL + '/api/roles' , data, {headers : authHeader()})
            .then((response) => {
                return response.data;
            });
    }
    updateNewRoles(data){
        console.log('data',data.id)
        return axios.put(API_URL + '/api/roles/'+  data.id, data, {headers : authHeader()})
            .then((response) => {
                return response.data;
            });
    }
    deleteRole(id){
        return axios.delete(API_URL + '/api/roles/' + id, {headers : authHeader()})
            .then((response) => {
                return response.data;
            });
    }
   


}

export default new RoleService();