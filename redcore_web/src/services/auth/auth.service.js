import axios from 'axios';

const API_URL = import.meta.env.VITE_API_URL;

class AuthService {
    login(user) {
        return axios.post(API_URL + '/api/auth/login', {
            email : user.email,
            password : user.password,
        })
            .then((response) => {
                if (response.data.access_token) {
                    localStorage.setItem('auth', JSON.stringify(response.data));
                }
              
                return response.data;
            })
    }

    logout() {
        localStorage.removeItem('auth');
    }

    register(user) {
        return axios.post(API_URL + '/api/auth/register', {
            name: user.name,
            email: user.email,
            password: user.password,
            password_confirmation: user.password_confirmation
        })
    }
}

export default new AuthService();