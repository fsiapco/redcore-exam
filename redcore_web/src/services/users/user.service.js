import axios from 'axios';
import authHeader from "@/services/auth/auth-header"

const API_URL = import.meta.env.VITE_API_URL;

class UserService {
    getAllUsers() {
        return axios.get(API_URL + '/api/users', {headers : authHeader()})
            .then((response) => {
                return response.data;
            });
    }
    createNewUsers(data){
        return axios.post(API_URL + '/api/users' , data, {headers : authHeader()})
            .then((response) => {
                return response.data;
            });
    }
    updateNewUsers(data){
        console.log('data',data.id)
        return axios.put(API_URL + '/api/users/'+  data.id, data, {headers : authHeader()})
            .then((response) => {
                return response.data;
            });
    }
    deleteUsers(id){
        return axios.delete(API_URL + '/api/users/' + id, {headers : authHeader()})
            .then((response) => {
                return response.data;
            });
    }
   


}

export default new UserService();