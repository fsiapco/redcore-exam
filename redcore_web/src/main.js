import {createApp} from 'vue'
import './tailwind.css'
import App from './App.vue'
import {routes} from './routes.js'
import {createRouter, createWebHistory} from 'vue-router'
import store from './store'

const app = createApp(App)

const router = createRouter({
    history : createWebHistory(),
    routes,
})

router.beforeEach((to, from, next) => {
    const guestPages = [
        '/login',
        '/register',
    ];

    const authRequired = !guestPages.includes(to.path);
    const loggedIn = localStorage.getItem('auth');

    if (authRequired && !loggedIn) {
        next('/login');
    } else {
        next();
    }
})

app.use(router)
app.use(store)
app.mount('#app')
