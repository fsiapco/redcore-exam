
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import Dashboard from './views/Dashboard.vue'
import Role from './views/Role.vue'
import User from './views/User.vue'
/** @type {import('vue-router').RouterOptions['routes']} */
export const routes = [
    {
        path : '/',
        component : Dashboard,
    },
    {
        path : '/role',
        component : Role,
    },
    {
        path : '/user',
        component : User,
    },
    {
        path : '/login',
        component : Login,
    },
    {
        path : '/register',
        component : Register,
    },
    
]
