import UserService from '../services/users/user.service';

const initialState = {
    jobs : null,
    userDetails: null,
    user:'',
    error: false
};

export const users = {
    namespaced: true,
    state: initialState,
    actions: {
        getAllUsers({commit}){
            return UserService.getAllUsers()
            .then(response => {
                commit('userDetails', response);
            }, error => {
                commit('userError');
            });
        },
        createNewUsers({commit},data){
        
            return UserService.createNewUsers(data)
            .then(response => {
                commit('userNewDetails', response);
            }, error => {
                commit('userError');
            });
        },
        updateNewUsers({commit},data){
            console.log('updateNewUsers',data)
            return UserService.updateNewUsers(data)
            .then(response => {
                commit('userNewDetails', response);
            }, error => {
                commit('userError');
            });
        },
        deleteUsers({commit},id){
            return UserService.deleteUsers(id)
            .then(response => {
                commit('userNewDetails', response);
            }, error => {
                commit('userError');
            });
        }

    },
    mutations: {
        userDetails(state, data) {
            state.userDetails = data;
            state.error = false;
        },
        userNewDetails(state, data) {
            state.user = data;
            state.error = false;
        },

        userError(state) {
            state.error = true;
        },
    }
}