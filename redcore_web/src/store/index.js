import {auth} from './auth.module';
import {jobs} from './jobs.module';
import {createStore} from 'vuex';
import {users} from './user.module';
import {roles} from './role.module';
const store = createStore({
    modules: {
        auth,
        jobs,
        users,
        roles
    }
});

export default store;