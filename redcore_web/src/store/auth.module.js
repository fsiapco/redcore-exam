import AuthService from '../services/auth/auth.service';

const user = JSON.parse(localStorage.getItem('auth'));

const initialState = user ? {
    api_url: import.meta.env.VITE_API_URL,
    status : {
        loggedIn : true,
    },
    user,
} : {
    status : {
        loggedIn : false,
    },
    user : null,
    api_url: import.meta.env.VITE_API_URL,
}

export const auth = {
    namespaced: true,
    state: initialState,
    actions: {
        login({commit}, user) {
            return AuthService.login(user)
                .then(user => {
                    commit('loginSuccess', user);

                    return Promise.resolve(user);
                }, error => {
                    commit('loginFailure');

                    return Promise.reject(error);
                })
        },

        logout({commit}) {
            AuthService.logout();
            commit('logout');
        },

        register({commit}, user) {
            return AuthService.register(user)
                .then(response => {
                    commit('registerSuccess');

                    return Promise.resolve(response.data);
                }, error => {
                    commit('registerFailure');

                    return Promise.reject(error);
                })
        },
    },
    mutations: {
        loginSuccess(state, user) {
            state.status.loggedIn = true;
            state.user = user;
        },

        loginFailure(state) {
            state.status.loggedIn = false;
            state.user = null;
        },

        logout(state) {
            state.status.loggedIn = false;
            state.user = null;
        },

        registerSuccess(state) {
            state.status.loggedIn = false;
        },

        registerFailure(state) {
            state.status.loggedIn = false;
        },
    }
}