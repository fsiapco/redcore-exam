import RoleService from '../services/role/role.service';

const initialState = {
    role : null,
    roleDetails: null,
    user:'',
    error: false
};

export const roles = {
    namespaced: true,
    state: initialState,
    actions: {
        getAllRoles({commit}){
            return RoleService.getAllRoles()
            .then(response => {
                commit('roleDetails', response);
            }, error => {
                commit('roleError');
            });
        },
        createNewRoles({commit},data){
            return RoleService.createNewRoles(data)
            .then(response => {
                commit('NewRoleDetails', response);
            }, error => {
                commit('roleError');
            });
        },
        updateNewRoles({commit},data){
            console.log('updateNewRoles',data)
            return RoleService.updateNewRoles(data)
            .then(response => {
                commit('NewRoleDetails', response);
            }, error => {
                commit('roleError');
            });
        },
        deleteRole({commit},id){
            return RoleService.deleteRole(id)
            .then(response => {
                commit('NewRoleDetails', response);
            }, error => {
                commit('roleError');
            });
        }

    },
    mutations: {
        roleDetails(state, data) {
            state.roleDetails = data;
            state.error = false;
        },
        NewRoleDetails(state, data) {
            state.roles = data;
            state.error = false;
        },

        roleError(state) {
            state.error = true;
        },
    }
}