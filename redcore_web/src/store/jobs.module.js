import JobsService from '../services/jobs/jobs.service';

const initialState = {
    jobs : null,
    jobsDetails: null,
    relatedJobs: null,
    recommendedJobs: null,
    filterJobs : null,
    location : null,
    jobLevels : null,
    jobTypes: null,
    educations: null,
    industries: null,
    departments: null,
    highestPayingJob : null,
    error: false
};

export const jobs = {
    namespaced: true,
    state: initialState,
    actions: {
        getJobsDetails({commit}){
            return JobsService.getJobsDetails()
            .then(response => {
                commit('jobsDetails', response);
            }, error => {
                commit('jobsError');
            });
        },

        getRecommendedJobs({commit}){
            return JobsService.getRecommendedJobs()
                .then(response => {
                    commit('recommendedJobs', response);
                }, error => {
                    commit('jobsError');
                });
        },

        getMostRecentJobs({commit}) {
            return JobsService.getMostRecentJobs()
                .then(response => {
                    commit('jobsCommit', response);
                }, error => {
                    commit('jobsError');
                });
        },

        getHighestPayingJob({commit}) {
            return JobsService.getHighestPayingJob()
                .then(response => {
                    commit('highestPayingJobCommit', response);
                }, error => {
                    commit('jobsError');
                })
        },
        getTrendingJobs({commit}){
            return JobsService.getTrendingJobs()
                        .then(response => {
                            commit('trendingJobs', response);
                        }, error => {
                            commit('jobsError');
                        })
        },
        getRelatedJobs({commit}){
            return JobsService.getRelatedJobs()
                        .then(response => {
                            commit('relatedJobs', response);
                        }, error => {
                            commit('jobsError');
                        })
        },
        searchJobs({commit},q){
            return JobsService.searchJobs(q)
                .then(response => {
                    commit('jobsSearch', response);
                }, error => {
                    commit('jobsError');
                });
        },
        getLocation({commit}){
            return JobsService.location()
            .then(response => {
                commit('location', response);
            }, error => {
                commit('jobsError');
            });
        },
        getjobLevels({commit}){
            return JobsService.jobLevels()
            .then(response => {
                commit('jobLevels', response);
            }, error => {
                commit('jobsError');
            });
        },
        getjobTypes({commit}) {
            return JobsService.jobTypes()
            .then(response => {
                commit('jobTypes', response);
            }, error => {
                commit('jobsError');
            });
        },
        getEducation({commit}) {
            return JobsService.education()
            .then(response => {
                commit('education', response);
            }, error => {
                commit('jobsError');
            });
        },
        getIndustry({commit}) {
            return JobsService.industry()
            .then(response => {
                commit('industry', response);
            }, error => {
                commit('jobsError');
            });
        },
        getDepartment({commit}) {
            return JobsService.department()
            .then(response => {
                commit('department', response);
            }, error => {
                commit('jobsError');
            });
        }
    },
    mutations: {
        jobsDetails(state, data) {
            state.jobsDetails = data;
            state.error = false;
        },
        jobsCommit(state, data) {
            state.jobs = data;
            state.error = false;
        },
        recommendedJobs(state, data){
            state.recommendedJobs = data;
            state.error = false;
        },
        trendingJobs(state, data) {
            state.jobs = data;
            state.error = false;
        },
        relatedJobs(state, data) {
            state.relatedJobs = data;
            state.error = false;
        },
        jobsSearch(state, data) {
            state.filterJobs = data;
            state.error = false;
        },
        jobLevels(state, data){
            state.jobLevels = data;
            state.error = false;
        },
        location(state, data){
            state.location = data;
            state.error = false;
        },
        jobTypes(state, data){
            state.jobTypes = data;
            state.error = false;
        },
        education(state, data) {
            state.educations = data;
            state.error = false;
        },
        industry(state, data) {
            state.industries = data;
            state.error = false;
        },
        department(state, data) {
            state.departments = data;
            state.error = false;
        },
        jobsError(state) {
            state.error = true;
        },

        highestPayingJobCommit(state, data) {
            state.highestPayingJob = data[0];
            state.error = false;
        },
    }
}